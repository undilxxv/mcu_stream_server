/* Importation du framework web koa (equivalent express) */
import koa from "koa";
/* Permet de récupérer l'extension de notre  */
import { extname, resolve } from "path";
/* Permet de créer un stream de lecture package fs de nodejs */
import { createReadStream, stat } from "fs";

import { promisify } from "util";
/* Import de jwt pour token premium */
/* import jwt from 'koa-jwt';
import { request } from "http";
 */
/* Construction de mon app Koa */
const app = new koa();

/* middleware pour jwt */
/* app.use(jwt({
    secret: 'your-256-bit-secret',
    algorithms: ['HS256', 'HS512'],
    getToken: ({request}) => request.query.token,

}))  */

/* middleware */
app.use(async ({ request, response }, next) => {
  /* si l'url n'est pas demandé avec '/api/video' on next() */
  if (
    !request.url.startsWith("/api/video") ||
    /* ou si la demande n'est pas une video */
    !request.query.video ||
    /* si la video ne match pas avec les caractères et l'extension */
    !request.query.video.match(/^[a-zA-Z0-9-_ ]+\.(mp4|mov)$/i)
    /* si quelqu'un essaye d'ouvrir la video dans un new onglet il obtient not found */
    /* pour éviter le téléchargement */
    //!request.header.range
  ) {
    console.log("marche pas ici");
    return next();
  }

  /* variable pour la demande de video */
  const video = resolve("videos", request.query.video);
  /* Recupere l'entete range du navigateur */
  const range = request.header.range;
  /* Au cas ou quelqu'un essai de charger l'url directement */
  if (!range) {
    /* Renvoi l'extension de notre video */
    response.type = extname(video);
    /* Renvoi notre contenu en modifiant la propriété body */
    response.body = createReadStream(video);
    return next();
  }

  /* Ce qui va nous creer un tableau de taille 2 */
  /* replace la string 'bytes=' par du vide ''  */
  /* split va donc diviser la string d'élément séparés par un -  */
  const parts = range.replace('bytes=', '').split('-');
  const videoStat = await promisify(stat)(video);
  /* Point de départ */
  /* On utilise parseInt pour transformer l'élément string du tableau créer par split en int de base 10 */
  /* cet élément va etre l'octet de départ pour demmarer notre video */
  const start = parseInt(parts[0],10);
  /* cet élément sera l'octet de fin si il existe dans range sinon la taille de la video -1 car l'octet commence a 0 */
  const end = parts[1] ? parseInt(parts[1],10) : videoStat.size -1;
  response.set('Content-Range', `bytes ${start}-${end}/${videoStat.size}`);
  response.set('Accept-Range', `bytes`);
  response.set('Content-Length', end - start + 1);
  /* réponse partielle   */
  response.status = 206;
  /* Renvoi donc notre stream sans télécharger la video entière   */
  response.body = createReadStream(video, {start, end});

  console.log(videoStat);
  console.log(request.query.video);
});



//permet d'écouter sur le port 3000
app.listen(3000);
