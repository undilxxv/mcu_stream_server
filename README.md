# SERVEUR STREAMING MCU

[![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)](http://forthebadge.com)  [![forthebadge](https://forthebadge.com/images/badges/powered-by-coffee.svg)](https://forthebadge.com)

Projet serveur de streaming nodejs

## Démarrage

Dites comment faire pour lancer votre projet

## Fabriqué avec

* [Nodejs](https://nodejs.org/) - Environnement d'execution JavaScript 
* [koa](https://koajs.com/) - next generation web framework for nodejs
* [VS Code]() - Le meilleur IDE de la terre


## Versions

**Dernière version :** 1.0.0
Liste des versions : [Cliquer pour afficher](https://gitlab.com/westindev/mcu_stream_server/tags)

## Auteurs

* **Axel Labarre** _alias_ [@westindev](https://gitlab.com/undilxxv)

## License

Ce projet est sous licence ``MIT`` 

